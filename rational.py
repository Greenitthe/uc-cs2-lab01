'''
Creator: James Clague
Date: 
Class: CPTR 227 01 - CS II
Description: Implement rational number class
'''

def eGCD(x, y):
		while y != 0:
			(x, y) = (y, x % y)
		return x

def get_input():
	user_1 = input("r1: ").split("/")
	user_2 = input("r2: ").split("/")
	rational_1 = Rational(int(user_1[0]), int(user_1[1]))
	rational_2 = Rational(int(user_2[0]), int(user_2[1]))
	print("{:>23}{}".format("Sum: " , rational_1 + rational_2))
	print("{:>23}{}".format("Difference: " , rational_1 - rational_2))
	print("{:>23}{}".format("Product: " , rational_1 * rational_2))
	print("{:>23}{}".format("Quotient: " , rational_1 / rational_2))
	print("{:>23}{}".format("less than: " , rational_1 < rational_2))
	print("{:>23}{}".format("less than or equal: " , rational_1 <= rational_2))
	print("{:>23}{}".format("greater than: " , rational_1 > rational_2))
	print("{:>23}{}".format("greater than or equal: " , rational_1 >= rational_2))
	print("{:>23}{}".format("equal to: " , rational_1 == rational_2))
	print("{:>23}{}".format("not equal to: " , rational_1 != rational_2))
	return

def ratLCD(rat1,rat2):
	#finds LCD for given rats
	#pre: rat1,2 are rationals
	#post: returns num_rat1, num_rat2, common_den
	num1 = rat1.num * rat2.den
	num2 = rat2.num * rat1.den
	cden = rat1.den * rat2.den
	return [num1, num2, cden]

class Rational(object):
	
	def __init__(self, num = 0, den = 1):
		#create new Rational obj
		#pre: num and den are int
		#post: creates rational obj s.t. num / den
		
		gcd = eGCD(num, den)
		self.num = int(num / gcd)
		self.den = int(den / gcd)
	
	def __add__(self, other):
		#+ operator
		#pre: self and other are rationals
		#post: return rational sum: self + other
		LCRATS = ratLCD(self, other)
		return Rational(LCRATS[0] + LCRATS[1], LCRATS[2])
		
	def __sub__(self, other):
		#- operator
		#pre: self and other are rationals
		#post: return rational difference: self - other
		LCRATS = ratLCD(self, other)
		return Rational(LCRATS[0] - LCRATS[1], LCRATS[2])
	
	def __mul__(self, other):
		#* operator
		#pre: self and other are rationals
		#post: return rational product: self * other
		
		num = self.num * other.num
		den = self.den * other.den
		return Rational(num, den)
		
	def __truediv__(self, other):
		#/ operator
		#pre: self and other are rationals
		#post: return rational quotient: self / other
		return self * Rational(other.den, other.num)
		
	def __lt__(self, other):
		#< operator
		#pre: self and other are rationals
		#post: return t/f of: self < other
		LCRATS = ratLCD(self, other)
		return LCRATS[0] < LCRATS[1]
	
	def __le__(self, other):
		#<= operator
		#pre: self and other are rationals
		#post: return t/f of: self <= other
		LCRATS = ratLCD(self, other)
		return LCRATS[0] <= LCRATS[1]
		
	def __gt__(self, other):
		#> operator
		#pre: self and other are rationals
		#post: return t/f of: self > other
		LCRATS = ratLCD(self, other)
		return LCRATS[0] > LCRATS[1]
		
	def __ge__(self, other):
		#>= operator
		#pre: self and other are rationals
		#post: return t/f of: self >= other
		LCRATS = ratLCD(self, other)
		return LCRATS[0] >= LCRATS[1]
	
	def __eq__(self, other):
		#== operator
		#pre: self and other are rationals
		#post: return t/f of: self == other
		LCRATS = ratLCD(self, other)
		return LCRATS[0] == LCRATS[1]
		
	def __ne__(self, other):
		#!= operator
		#pre: self and other are rationals
		#post: return t/f of: self != other
		LCRATS = ratLCD(self, other)
		return LCRATS[0] != LCRATS[1]
		
	def __str__(self):
		#ret str for printing
		#pre: self as Rational
		#post: ret str representation of self
		return str(self.num) + '/' + str(self.den)

def main(args):
    return 0

if __name__ == '__main__':
	import sys
	while True:
		get_input()
