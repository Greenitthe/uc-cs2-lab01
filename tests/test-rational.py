import sys
import unittest

sys.path.insert(0, '..')
from rational import *

class MainTest(unittest.TestCase):
    rat1 = Rational(1,2)
    rat2 = Rational(1,4)
    def testAdd(self):
        self.assertEqual(self.rat1 + self.rat2, Rational(3,4))
    def testSubtract(self):
        self.assertEqual(self.rat1 - self.rat2, Rational(1,4))
    def testMultiply(self):
        self.assertEqual(self.rat1 * self.rat2, Rational(1,8))
    def testDivide(self):
        self.assertEqual(self.rat1 / self.rat2, Rational(2,1))
    def testLT(self):
        self.assertEqual(self.rat2 < self.rat1, True)
    def testLE(self):
        self.assertEqual(self.rat1 <= Rational(1,2), True)
        self.assertEqual(self.rat1 <= Rational(1,1), True)
    def testGT(self):
        self.assertEqual(self.rat1 > self.rat2, True)
    def testGE(self):
        self.assertEqual(self.rat1 >= Rational(1,2), True)
        self.assertEqual(self.rat1 >= self.rat2, True)
    def testEQ(self):
        self.assertEqual(self.rat1 == Rational(1,2), True)
    def testNE(self):
        self.assertEqual(self.rat1 != self.rat2, True)
    
def main(args):
    unittest.main()

if __name__ == '__main__':
    main(sys.argv)
